commands = map(str.split, open("input.txt").readlines())

x, y = 0, 0
for command in commands:
    match command:
        case ["forward", units]:
            x += int(units)
        case ["up", units]:
            y -= int(units)
        case ["down", units]:
            y += int(units)

print(x * y)
