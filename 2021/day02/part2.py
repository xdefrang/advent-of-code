def parse(line):
    command, units = line.split()
    return (command, int(units))


commands = map(parse, open("input.txt").readlines())

x, y, aim = 0, 0, 0
for command in commands:
    match command:
        case ("forward", units):
            x += units
            y += units * aim
        case ("up", units):
            aim -= units
        case ("down", units):
            aim += units

print(x * y)
