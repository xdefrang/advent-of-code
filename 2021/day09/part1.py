heightmap = [list(map(int, line.strip())) for line in open("input.txt").readlines()]

height, width = len(heightmap), len(heightmap[0])


def neighbors(x, y):
    if y > 0:
        yield (x, y - 1)
    if x < (width - 1):
        yield (x + 1, y)
    if y < (height - 1):
        yield (x, y + 1)
    if x > 0:
        yield (x - 1, y)


local_minimums = []

for y in range(height):
    for x in range(width):
        h = heightmap[y][x]
        if all(h < heightmap[ny][nx] for nx, ny in neighbors(x, y)):
            local_minimums.append(h)

print(sum((minimum + 1 for minimum in local_minimums)))
