import functools
import operator
import typing


heightmap = [list(map(int, line.strip())) for line in open("input.txt").readlines()]

height, width = len(heightmap), len(heightmap[0])


class Point(typing.NamedTuple):
    x: int
    y: int


def neighbors(p):
    if p.y > 0:
        yield Point(p.x, p.y - 1)
    if p.x < (width - 1):
        yield Point(p.x + 1, p.y)
    if p.y < (height - 1):
        yield Point(p.x, p.y + 1)
    if p.x > 0:
        yield Point(p.x - 1, p.y)


def local_minimums():
    for y in range(height):
        for x in range(width):
            h = heightmap[y][x]
            p = Point(x, y)
            if all(h < heightmap[ny][nx] for nx, ny in neighbors(p)):
                yield p


def basin_size(p):
    def calculate(p, visited):
        h = heightmap[p.y][p.x]
        if h == 9:
            return 0

        new_basin_members = (
            n for n in neighbors(p) if h < heightmap[n.y][n.x] and n not in visited
        )

        visited.add(p)

        return 1 + sum(calculate(n, visited) for n in new_basin_members)

    return calculate(p, set())


basin_sizes = sorted(map(basin_size, local_minimums()), reverse=True)

print(
    functools.reduce(
        operator.__mul__,
        basin_sizes[:3],
    ),
)
