from collections import Counter
from typing import NamedTuple
import re


class Point(NamedTuple):
    x: int
    y: int

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)


class Line(NamedTuple):
    a: Point
    b: Point

    def should_be_considered(self):
        return self.is_horizontal() or self.is_vertical()

    def is_horizontal(self):
        return self.a.y == self.b.y

    def is_vertical(self):
        return self.a.x == self.b.x

    def manhattan_points(self):
        point, target = sorted(self)
        direction = Point(1, 0) if self.is_horizontal() else Point(0, 1)

        yield point

        while point != target:
            point += direction
            yield point

    @classmethod
    def parse(cls, line):
        if m := re.match(r"(\d+),(\d+) -> (\d+),(\d+)", line):
            return Line(
                Point(int(m[1]), int(m[2])),
                Point(int(m[3]), int(m[4])),
            )
        raise ValueError(line)


def parse(filename):
    return list(map(Line.parse, open(filename).readlines()))


lines = parse("input.txt")

grid_aligned_lines = [line for line in lines if line.should_be_considered()]

counter = Counter(
    point for line in grid_aligned_lines for point in line.manhattan_points()
)

print(sum(value > 1 for value in counter.values()))
