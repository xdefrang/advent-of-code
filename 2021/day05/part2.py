from collections import Counter
from typing import NamedTuple
import re


class Point(NamedTuple):
    x: int
    y: int

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def pseudonormalize(self):
        return Point(
            self._pseudonormalize(self.x),
            self._pseudonormalize(self.y),
        )

    @staticmethod
    def _pseudonormalize(i):
        return (i > 0) - (i < 0)


class Line(NamedTuple):
    a: Point
    b: Point

    def should_be_considered(self):
        dx = abs(self.a.x - self.b.x)
        dy = abs(self.a.y - self.b.y)
        return dx == 0 or dy == 0 or dx == dy

    def points(self):
        point, target = sorted(self)
        direction = (target - point).pseudonormalize()

        yield point

        while point != target:
            point += direction
            yield point

    @classmethod
    def parse(cls, line):
        if m := re.match(r"(\d+),(\d+) -> (\d+),(\d+)", line):
            return Line(
                Point(int(m[1]), int(m[2])),
                Point(int(m[3]), int(m[4])),
            )
        raise ValueError(line)


def parse(filename):
    return list(map(Line.parse, open(filename).readlines()))


lines = parse("input.txt")

counter = Counter(
    point for line in lines if line.should_be_considered() for point in line.points()
)

print(sum(value > 1 for value in counter.values()))
