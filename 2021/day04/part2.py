class Board:
    def __init__(self, numbers):
        self.numbers = numbers
        self.size = len(self.numbers)
        self.marked = set()

    def mark(self, number_drawn):
        for y, row in enumerate(self.numbers):
            for x, number in enumerate(row):
                if number == number_drawn:
                    self.marked.add((x, y))

    def is_marked(self, x, y):
        return (x, y) in self.marked

    def is_bingo(self):
        for i in range(self.size):
            if all(self.is_marked(j, i) for j in range(self.size)):
                return True

            if all(self.is_marked(i, j) for j in range(self.size)):
                return True

        if all(self.is_marked(i, i) for i in range(self.size)):
            return True

        if all(self.is_marked(i, self.size - i) for i in range(self.size)):
            return True

        return False

    def score(self, drawn_number):
        return sum(self.unmarked_numbers()) * drawn_number

    def unmarked_numbers(self):
        for y, row in enumerate(self.numbers):
            for x, number in enumerate(row):
                if not self.is_marked(x, y):
                    yield number


def parse(filename):
    contents = open(filename).read()

    numbers_line, *all_boards_lines = contents.split("\n\n")

    numbers = list(map(int, numbers_line.split(",")))

    boards = [
        Board(
            [list(map(int, line.split())) for line in boards_lines.strip().split("\n")]
        )
        for boards_lines in all_boards_lines
    ]

    return numbers, boards


numbers, boards = parse("input.txt")

winnable_boards = set(range(len(boards)))

for number in numbers:
    for index, board in enumerate(boards):
        if index not in winnable_boards:
            continue

        board.mark(number)

        if board.is_bingo():
            if len(winnable_boards) == 1:
                print("Bingo!", index, board.score(number))
                exit(0)
            else:
                winnable_boards.remove(index)
