import sys

crabs = list(map(int, open("input.txt").read().strip().split(",")))

min_pos, max_pos = min(crabs), max(crabs)

best_position, best_fuel = None, sys.maxsize
for position in range(min_pos, max_pos + 1):
    fuel_used = sum(abs(position - p) for p in crabs)
    if fuel_used < best_fuel:
        best_position, best_fuel = position, fuel_used

print(f"{best_position=}, {best_fuel=}")
