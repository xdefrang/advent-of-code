import itertools

depths = map(int, open("input.txt").readlines())


def sliding_window(seq, size=3):
    it = iter(seq)
    window = tuple(itertools.islice(it, size))
    if len(window) == size:
        yield window
    for elem in it:
        window = window[1:] + (elem,)
        yield window


print(sum(s1 < s2 for s1, s2 in itertools.pairwise(map(sum, sliding_window(depths)))))
