import itertools

depths = map(int, open("input.txt").readlines())

print(sum(d1 < d2 for d1, d2 in itertools.pairwise(depths)))
