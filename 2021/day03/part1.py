from collections import Counter

numbers = list(tuple(map(int, line.strip())) for line in open("input.txt").readlines())

counters = tuple(Counter() for _ in range(len(numbers[0])))

for number in numbers:
    for index, bit in enumerate(number, start=0):
        counters[index].update((bit,))

gamma, epsilon = 0, 0

for counter in counters:
    [(gamma_bit, _), (epsilon_bit, _)] = counter.most_common()
    gamma = (gamma << 1) + gamma_bit
    epsilon = (epsilon << 1) + epsilon_bit

print(gamma * epsilon)
