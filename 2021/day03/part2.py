import itertools
from functools import reduce

numbers = list(tuple(map(int, line.strip())) for line in open("input.txt").readlines())


def partition(items, predicate):
    a, b = itertools.tee((predicate(item), item) for item in items)
    return ([item for pred, item in a if pred], [item for pred, item in b if not pred])


def partition_by_bit(numbers, bit_index):
    return partition(numbers, lambda number: number[bit_index] == 0)


def binary_number(bits):
    return reduce(lambda number, bit: (number << 1) + bit, bits, 0)


o2_numbers, co2_numbers = numbers, numbers[:]
o2_rating, co2_rating = None, None

bit_index = 0
while o2_rating is None or co2_rating is None:
    o2_0, o2_1 = partition_by_bit(o2_numbers, bit_index)
    co2_0, co2_1 = partition_by_bit(co2_numbers, bit_index)

    o2_numbers = o2_0 if len(o2_0) > len(o2_1) else o2_1
    co2_numbers = co2_0 if len(co2_0) <= len(co2_1) else co2_1

    if o2_rating is None and len(o2_numbers) == 1:
        o2_rating = binary_number(o2_numbers[0])

    if co2_rating is None and len(co2_numbers) == 1:
        co2_rating = binary_number(co2_numbers[0])

    bit_index += 1

print(o2_rating * co2_rating)
