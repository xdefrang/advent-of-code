class Fish:
    def __init__(self, timer):
        self.timer = timer

    def tick(self):
        if self.timer > 0:
            self.timer -= 1
            return False
        else:
            self.timer = 6
            return True

    def __repr__(self):
        return f"{self.timer}"


class School:
    def __init__(self, fishes):
        self.fishes = fishes

    def tick(self):
        new_fishes = []
        for fish in self.fishes:
            if fish.tick():
                new_fishes.append(Fish(8))
        self.fishes.extend(new_fishes)

    def __len__(self):
        return len(self.fishes)

    def __repr__(self):
        return ",".join(map(repr, self.fishes))


school = School([Fish(int(n)) for n in open("input.txt").read().split(",")])


for day in range(80):
    # print(f"{day}: {school!r}")
    school.tick()


print(len(school))
