from collections import Counter, defaultdict

counters = defaultdict(int, Counter(map(int, open("input.txt").read().split(","))))

for day in range(256):
    spawns = counters[0]
    counters[0] = counters[1]
    counters[1] = counters[2]
    counters[2] = counters[3]
    counters[3] = counters[4]
    counters[4] = counters[5]
    counters[5] = counters[6]
    counters[6] = counters[7] + spawns
    counters[7] = counters[8]
    counters[8] = spawns

print(sum(counters.values()))
