lines = map(str.strip, open("input.txt").readlines())

character_scores = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}

character_pairs = {
    "(": ")",
    "[": "]",
    "{": "}",
    "<": ">",
}


def syntax_error_score(line):
    stack = []
    for character in line:
        if character in character_pairs:
            stack.append(character)
        elif character != character_pairs[stack.pop()]:
            return character_scores[character]
    return 0


print(sum(syntax_error_score(line) for line in lines))
