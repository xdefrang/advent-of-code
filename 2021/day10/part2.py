import functools
import statistics

lines = map(str.strip, open("input.txt").readlines())

character_scores = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
}

character_pairs = {
    "(": ")",
    "[": "]",
    "{": "}",
    "<": ">",
}


def syntax_completion_score(line):
    stack = []
    for character in line:
        if character in character_pairs:
            stack.append(character)
        elif character != character_pairs[stack.pop()]:
            return 0

    return functools.reduce(
        lambda acc, char: acc * 5 + character_scores[character_pairs[char]],
        reversed(stack),
        0,
    )


scores = sorted(
    score for score in (syntax_completion_score(line) for line in lines) if score != 0
)

print(scores[len(scores) // 2])
