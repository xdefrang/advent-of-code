INPUT = File.read("input.txt").split.freeze

TEST_PART1 = [
  "abcdef",
  "bababc",
  "abbcde",
  "abcccd",
  "aabcdd",
  "abcdee",
  "ababab"
]

TEST_PART2 = [
  "abcde",
  "fghij",
  "klmno",
  "pqrst",
  "fguij",
  "axcye",
  "wvxyz"
]

class Hash

  def self.counter
    new(0)
  end

  def self.collector
    new { |h, k| h[k] = [] }
  end

end

def analyze_box_id(box_id)
  box_id
    .chars
    .each_with_object(Hash.counter) { |c, h| h[c] += 1 }
    .each_with_object(Hash.collector) { |(k, v), h| h[v] << k }
end

def part1(box_ids)
  box_ids
    .each_with_object({ 2 => 0, 3 => 0 }) do |box_id, factors|
      analysis = analyze_box_id(box_id)
      factors[2] += 1 if analysis[2].size > 0
      factors[3] += 1 if analysis[3].size > 0
    end
    .values
    .reduce(:*)
end

def find_index_of_single_diff(a, b)
  diff_index = -1
  a.size.times do |idx|
    if a[idx] != b[idx]
      if diff_index > -1
        return -1
      else
        diff_index = idx
      end
    end
  end
  diff_index
end

def part2(box_ids)
  box_ids.each do |box_id|
    box_ids.each do |other_box_id|
      if (idx = find_index_of_single_diff(box_id, other_box_id)) > -1
        box_id.slice!(idx)
        return box_id
      end
    end
  end
  return nil
end

p part1(TEST_PART1)
p part1(INPUT)
p part2(TEST_PART2)
p part2(INPUT)

