defmodule Day01 do
  def part1(input) do
    Enum.reduce(input, &Kernel.+/2)
  end

  def part2(input) do
    input
    |> Stream.cycle()
    |> Enum.reduce_while(
      {0, MapSet.new()},
      fn change, {freq, freqs} ->
        new_freq = freq + change

        if MapSet.member?(freqs, new_freq) do
          {:halt, new_freq}
        else
          {:cont, {new_freq, MapSet.put(freqs, freq)}}
        end
      end
    )
  end
end

input =
  "input.txt"
  |> File.read!()
  |> String.split()
  |> Enum.map(&String.to_integer/1)

IO.puts(Day01.part1(input))
IO.puts(Day01.part2(input))
