require "set"

INPUT = File.read("input.txt").split("\n").sort.freeze

TEST_PART1 = <<~EOT
[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up
EOT

class ReposeRecord

  attr_reader :dates

  def initialize
    @dates = Hash.new { |dates, date|
      dates[date] = Hash.new { |guards, guard|
        guards[guard] = [false] * 60
      }
    }
  end

  def parse(lines)
    guard, start_min = nil
    lines.each do |line|
      case line
      when /\[(\d{4}-\d{2}-\d{2}) (\d{2}):(\d{2})\] Guard #(\d+) begins shift/
        date, hour, min = $1, $2.to_i, $3.to_i
        guard = $4.to_i
        start_min = hour.zero? ? min : 0
      when /\[(\d{4}-\d{2}-\d{2}) (\d{2}):(\d{2})\] falls asleep/
        date, hour, min = $1, $2.to_i, $3.to_i
        start_min = min
      when /\[(\d{4}-\d{2}-\d{2}) (\d{2}):(\d{2})\] wakes up/
        date, hour, min = $1, $2.to_i, $3.to_i
        asleep(date, guard, start_min, min)
        start_min = min
      end
    end
    self
  end

  def to_s
    @dates.keys.sort.flat_map do |date|
      @dates[date].keys.sort.map do |guard|
        sleep_pattern = @dates[date][guard].map { |x| x ? "#" : "." }.join
        sprintf("%s %-5s %s", date, "##{guard}", sleep_pattern)
      end
    end.join("\n")
  end

  # Part 1

  def sleepiest_guard
    @dates
      .values
      .reduce(Hash.new(0)) do |minutes_asleep, guards|
        guards.reduce(minutes_asleep) do |acc, (guard, sleep_pattern)|
          acc[guard] += sleep_pattern.count(&:itself)
          acc
        end
      end
      .max_by { |_, mins| mins }
      .first
  end

  def sleepiest_minute(guard)
    @dates
      .values
      .reduce(Hash.new(0)) do |minutes, guards|
        guards.fetch(guard, []).each_with_index.reduce(minutes) do |acc, (asleep, minute)|
          acc[minute] += 1 if asleep
          acc
        end
      end
      .max_by { |_, total| total }
      .first
  end

  # Part 2

  # Returns [guard, total_minutes_asleep]
  def sleep_record_on_minute(minute)
    @dates
      .values
      .reduce(Hash.new(0)) do |minutes_asleep, guards|
        guards.reduce(minutes_asleep) do |acc, (guard, sleep_pattern)|
          acc[guard] += sleep_pattern.fetch(minute) ? 1 : 0
          acc
        end
      end
      .max_by { |_, mins| mins }
  end

  # Returns [guard, minute]
  def sleepiest_guard_on_same_minute
    60.times.reduce([nil, nil, 0]) do |acc, minute|
      guard, minutes_asleep = sleep_record_on_minute(minute)
      if minutes_asleep > acc[2]
        [guard, minute, minutes_asleep]
      else
        acc
      end
    end.take(2)
  end

  private

  def asleep(date, guard, from_minute, to_minute)
    sleep_pattern = @dates[date][guard]
    from_minute.upto(to_minute - 1) { |minute| sleep_pattern[minute] = true }
  end

end

# Test
test = ReposeRecord.new.parse(TEST_PART1.lines)
puts test
puts test_part1a = test.sleepiest_guard
puts test_part1b = test.sleepiest_minute(test_part1a)
puts test_part1a * test_part1b
test_part2a, test_part2b = test.sleepiest_guard_on_same_minute
puts test_part2a
puts test_part2b
puts test_part2a * test_part2b

puts "-" * 20

# Puzzle
puzzle = ReposeRecord.new.parse(INPUT)
puts part1a = puzzle.sleepiest_guard
puts part1b = puzzle.sleepiest_minute(part1a)
puts part1a * part1b
part2a, part2b = puzzle.sleepiest_guard_on_same_minute
puts part2a
puts part2b
puts part2a * part2b
