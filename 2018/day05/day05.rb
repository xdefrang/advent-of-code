INPUT = File.read("input.txt").strip.freeze

ALPHABET = "a".."z"
PATTERN_UNIT = ALPHABET.flat_map { |letter| "#{letter}#{letter.upcase}|#{letter.upcase}#{letter}" }.join("|")
REGEX_UNIT = Regexp.new(PATTERN_UNIT)

def react(polymer)
  while polymer.sub!(REGEX_UNIT, "") ; end
  polymer
end

def improve(polymer)
  ALPHABET.reduce(polymer) do |shortest, letter|
    candidate = react(polymer.gsub(/#{letter}/i, ""))
    if candidate.size < shortest.size
      candidate
    else
      shortest
    end
  end
end

# Test part1
puts react("dabAcCaCBAcCcaDA")

# Part 1
puts react(INPUT.dup).size
# => 11754

# Part 2
puts improve(INPUT).size
# => 4098
