require "set"

Rect = Struct.new(:id, :x, :y, :width, :height)

def parse(lines)
  lines.map { |line|
    line =~ /^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$/
    Rect.new(*[$1, $2, $3, $4, $5].map(&:to_i))
  }.freeze
end

class Fabric

  def initialize
    @matrix = Hash.new { |rows, i|
      rows[i] = Hash.new { |cols, j|
        cols[j] = []
      }
    }
  end

  def claim(rect)
    rect.height.times do |i|
      rect.width.times do |j|
        @matrix[rect.y+i][rect.x+j] << rect.id
      end
    end
    self
  end

  def each(&block)
    Enumerator.new do |enum|
      @matrix.each do |i, cols|
        cols.each do |j, val|
          enum.yield val
        end
      end
    end
  end

end

def part1(fabric)
  fabric
    .each
    .count { |rects| rects.size > 1 }
end

def part2(fabric)
  fabric
    .each
    .reduce([Set.new, Set.new]) { |(good, bad), rects|
      if rects.size == 1 && !bad.include?(rects.first)
        [good.add(rects.first), bad]
      else
        [good - rects, bad | rects]
      end
    }.first
end

TEST_PART1 = parse("""
#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2
""".strip.lines)

p part1(TEST_PART1.reduce(Fabric.new) { |fabric, rect| fabric.claim(rect) })

INPUT = parse(File.readlines("input.txt"))

fabric = INPUT.reduce(Fabric.new) { |fabric, rect| fabric.claim(rect) }
p part1(fabric)
p part2(fabric)
