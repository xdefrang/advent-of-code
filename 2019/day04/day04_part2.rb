
def valid?(password)
  password.scan(/(\d)(\1{1,})/).any? { |_, reps| reps.size == 1 } \
    && password.split("").each_cons(2).all? { |a, b| a <= b }
end

INPUT = (273025..767253).to_enum.map(&:to_s)

puts INPUT.count { |password| valid?(password) }
