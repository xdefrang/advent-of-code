
def valid?(password)
  pairs = password.split("").each_cons(2).to_a
  pairs.all? { |a, b| a <= b } && pairs.any? { |a, b| a == b }
end

INPUT = (273025..767253).to_enum.map(&:to_s)

puts INPUT.count { |password| valid?(password) }
