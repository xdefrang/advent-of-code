INPUT = File.read("input.txt").split(",").map(&:to_i).freeze

class Program

  def initialize(memory)
    @memory = memory
    @ip = 0
  end

  def run
    until step() == :halt; end
    self
  end

  def step
    case fetch()
    when 1
      src1, src2, tgt = fetch(), fetch(), fetch()
      a, b = peek(src1), peek(src2)
      poke(tgt, a + b)
    when 2
      src1, src2, tgt = fetch(), fetch(), fetch()
      a, b = peek(src1), peek(src2)
      poke(tgt, a * b)
    when 99
      :halt
    end
  end

  def fetch
    value = peek(@ip)
    @ip += 1
    value
  end

  def peek(address)
    @memory.fetch(address)
  end

  def poke(address, value)
    @memory[address] = value
  end

end

def patch(input, noun, verb)
  copy = input.dup
  copy[1] = noun
  copy[2] = verb
  copy
end

# Part 1
puts Program.new(patch(INPUT, 12, 2)).run.peek(0)

# Part 2
0.upto(99).each do |noun|
  0.upto(99).each do |verb|
    if Program.new(patch(INPUT, noun, verb)).run.peek(0) == 19690720
      puts noun * 100 + verb
    end
  end
end
