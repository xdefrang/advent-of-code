require "set"


Point = Struct.new(:x, :y) do

  def zero?
    x.zero? && y.zero?
  end

  def +(other)
    self.class.new(x + other.x, y + other.y)
  end

end

class Instruction

  DIRECTION_VECTORS = {
    "U" => Point.new(0, 1),
    "D" => Point.new(0, -1),
    "L" => Point.new(-1, 0),
    "R" => Point.new(1, 0)
  }

  attr_reader :direction, :distance

  def initialize(string)
    string =~ /\A([UDLR])(\d+)\z/
    @direction = DIRECTION_VECTORS.fetch($1)
    @distance = $2.to_i
  end

  def self.parse_line(input)
    input.strip.split(",").map { |s| new(s) }
  end

end

class Grid

  def initialize
    @locations = Set.new
    @intersections = Set.new
  end

  def add_wires(wires)
    wires.each { |instructions| add_wire(instructions) }
    self
  end

  def add_wire(instructions)
    location = Point.new(0, 0)
    current_wire_locations = Set.new
    instructions.each do |instruction|
      instruction.distance.times do
        @intersections << location if !location.zero? && @locations.include?(location) && !current_wire_locations.include?(location)
        current_wire_locations << location
        @locations << location
        location += instruction.direction
      end
    end
  end

  def closest_intersection
    @intersections.map { |p| p.x.abs + p.y.abs }.min
  end

end


puts Grid.new.add_wires([
  Instruction.parse_line("R8,U5,L5,D3"),
  Instruction.parse_line("U7,R6,D4,L4")
]).closest_intersection

puts Grid.new.add_wires([
  Instruction.parse_line("R75,D30,R83,U83,L12,D49,R71,U7,L72"),
  Instruction.parse_line("U62,R66,U55,R34,D71,R55,D58,R83")
]).closest_intersection

puts Grid.new.add_wires([
  Instruction.parse_line("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"),
  Instruction.parse_line("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
]).closest_intersection

INPUT = File.readlines("input.txt").map { |line| Instruction.parse_line(line) }

puts Grid.new.add_wires(INPUT).closest_intersection
