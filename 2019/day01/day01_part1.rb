INPUT = File.readlines("input.txt").map(&:to_i)

def fuel_requirement(mass)
  (mass / 3.0).floor - 2
end

puts fuel_requirement(12)
puts fuel_requirement(14)
puts fuel_requirement(1969)
puts fuel_requirement(100756)

puts INPUT.reduce(0) { |acc, m| acc + fuel_requirement(m) }
