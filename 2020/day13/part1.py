with open("input.txt") as f:
    timestamp = int(next(f))
    buses = [int(bus_id) for bus_id in next(f).split(",") if bus_id != "x"]


def find_earliest(timestamp, buses):
    t = timestamp
    while True:
        for bus_id in buses:
            if t % bus_id == 0:
                return (t - timestamp, bus_id)
        t += 1


(wait, bus_id) = find_earliest(timestamp, buses)

print(wait, bus_id, wait * bus_id)
