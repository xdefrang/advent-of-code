from functools import reduce
from operator import __mul__


with open("input.txt") as f:
    next(f)
    buses = [
        (int(bus_id), offset)
        for offset, bus_id in enumerate(next(f).split(","))
        if bus_id != "x"
    ]


def chinese_remainder(n, a):
    product = reduce(__mul__, n)
    return (
        sum(
            [
                a_i * pow(p, -1, n_i) * p
                for n_i, a_i, p in [
                    (n_i, a_i, product // n_i) for n_i, a_i in zip(n, a)
                ]
            ]
        )
        % product
    )


# What a bullshit puzzle...
print(
    chinese_remainder(
        [bus for bus, offset in buses], [bus - offset for bus, offset in buses]
    )
)
