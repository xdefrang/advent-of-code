from itertools import product

with open("input.txt") as f:
    numbers = [int(n) for n in f.readlines()]

[(a, b)] = [(a, b) for (a, b) in product(numbers, repeat=2) if a < b and a + b == 2020]

print(a * b)
