from itertools import product

with open("input.txt") as f:
    numbers = [int(n) for n in f.readlines()]

[(a, b, c)] = [
    (a, b, c)
    for (a, b, c) in product(numbers, repeat=3)
    if a < b < c and a + b + c == 2020
]

print(a * b * c)
