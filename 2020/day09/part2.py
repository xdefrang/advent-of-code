from itertools import product

input = [int(line) for line in open("input.txt")]


def find_message(numbers, key):
    max_index = len(numbers) - 1
    for index in range(0, max_index):
        end_index = index + 1
        while end_index < max_index:
            message = numbers[index:end_index]
            if sum(message) == key:
                return message
            else:
                end_index += 1


message = find_message(input, 1930745883)

print(min(message) + max(message))
