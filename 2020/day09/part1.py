from itertools import product

input = [int(line) for line in open("input.txt")]


def is_valid(n, preamble):
    return any([n1 for (n1, n2) in product(preamble, repeat=2) if (n1 + n2) == n])


def find_invalid(numbers, preamble_length=25):
    for index in range(preamble_length, len(numbers) - 1):
        n = numbers[index]
        if not is_valid(n, numbers[index - preamble_length : index]):
            return n


print(find_invalid(input))
