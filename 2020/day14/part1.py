import re

REGEX_MASK = re.compile(r"(mask) = ([01X]{36})")
REGEX_MEM = re.compile(r"(mem)\[(\d+)\] = (\d+)")


def parse_line(line):
    m = REGEX_MASK.match(line)
    if m:
        return (m[1], parse_mask(m[2]))

    m = REGEX_MEM.match(line)
    if m:
        return (m[1], int(m[2]), int(m[3]))

    raise ValueError(line)


def parse_mask(string):
    and_mask, or_mask = 0, 0
    for bit in string:
        and_mask = and_mask << 1 | int(bit in "X1")
        or_mask = or_mask << 1 | int(bit == "1")
    return (and_mask, or_mask)


def run_program(instructions):
    memory = {}
    and_mask, or_mask = 0, 0
    for ins in program:
        if ins[0] == "mask":
            and_mask, or_mask = ins[1]
        if ins[0] == "mem":
            memory[ins[1]] = (ins[2] & and_mask) | or_mask
    return memory


program = [parse_line(line) for line in open("input.txt")]

print(sum(run_program(program).values()))
