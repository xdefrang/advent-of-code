import re
from itertools import product
from functools import reduce

REGEX_MASK = re.compile(r"(mask) = ([01X]{36})")
REGEX_MEM = re.compile(r"(mem)\[(\d+)\] = (\d+)")


def parse_line(line):
    m = REGEX_MASK.match(line)
    if m:
        return (m[1], parse_mask(m[2]))

    m = REGEX_MEM.match(line)
    if m:
        return (m[1], int(m[2]), int(m[3]))

    raise ValueError(line)


def parse_mask(string):
    or_mask, floating_bit, floating_bits = 0, 1 << 35, []

    for bit in string:
        or_mask = or_mask << 1 | int(bit == "1")
        if bit == "X":
            floating_bits.append(floating_bit)
        floating_bit >>= 1

    return (or_mask, floating_bits)


def run_program(instructions):
    memory = {}
    or_mask, floating_bits = 0, []
    for ins in program:
        if ins[0] == "mask":
            or_mask, floating_bits = ins[1]
        if ins[0] == "mem":
            base_address = ins[1] | or_mask
            value = ins[2]
            for toggle_bits in product((0, 1), repeat=len(floating_bits)):
                masked_address = reduce(
                    lambda mask, bits: (mask & ~bits[0]) | (bits[0] * bits[1]),
                    zip(floating_bits, toggle_bits),
                    base_address,
                )
                memory[masked_address] = value

    return memory


program = [parse_line(line) for line in open("input.txt")]

print(sum(run_program(program).values()))
