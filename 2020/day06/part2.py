from functools import reduce
from operator import __and__

with open("input.txt") as f:
    groups = [group.strip().split("\n") for group in f.read().split("\n\n")]

    yes_questions = [
        reduce(
            __and__,
            [{question for question in questions} for questions in people],
        )
        for people in groups
    ]

print(sum([len(questions) for questions in yes_questions]))
