with open("input.txt") as f:
    group_questions = [
        {question for question in group.strip().replace("\n", "")}
        for group in f.read().split("\n\n")
    ]


print(sum([len(questions) for questions in group_questions]))
