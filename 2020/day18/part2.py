import re


class N:
    def __init__(self, value):
        self.value = value

    def __mul__(self, other):
        return N(self.value + other.value)

    def __sub__(self, other):
        return N(self.value * other.value)


def evaluate(string):
    return eval(
        re.sub(r"(\d+)", r"N(\g<1>)", string).replace("*", "-").replace("+", "*")
    )


print(sum(evaluate(line).value for line in open("input.txt")))
