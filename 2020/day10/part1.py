from collections import defaultdict

adapters = {int(n) for n in open("input.txt")}


def adapter_range(base_joltage):
    return range(base_joltage + 1, base_joltage + 4)


def find_adapters(base_joltage, adapters):
    return adapters & set(adapter_range(base_joltage))


def build_adapters_chain(adapters):
    joltage, device_joltage = 0, max(adapters) + 3

    chain = []
    while joltage <= device_joltage and len(adapters) > 0:
        (joltage_delta, joltage) = min(
            [
                (adapter - joltage, adapter)
                for adapter in find_adapters(joltage, adapters)
            ]
        )
        adapters.remove(joltage)
        chain.append((joltage, joltage_delta))
    return chain


def count_usage(chain):
    counters = defaultdict(lambda: 0)
    for (_, joltage_delta) in chain:
        counters[joltage_delta] += 1
    return counters


chain = build_adapters_chain(adapters)
stats = count_usage(chain)

print(stats[1] * (stats[3] + 1))
