from functools import lru_cache

adapters = {int(n) for n in open("input.txt")}


def adapter_range(base_joltage):
    return range(base_joltage + 1, base_joltage + 4)


def find_adapters(base_joltage, adapters):
    return adapters & set(adapter_range(base_joltage))


def count_all_combinations(adapters):
    device_joltage = max(adapters)

    @lru_cache
    def count_combinations(joltage):
        if joltage == device_joltage:
            return 1
        else:
            return sum(
                [count_combinations(a) for a in find_adapters(joltage, adapters)]
            )

    return count_combinations(0)


print(count_all_combinations(adapters))
