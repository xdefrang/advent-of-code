from collections import namedtuple
from functools import reduce

import re

REGEX_DIR = re.compile(r"(e|w|s[ew]|n[ew])")


def parse_instructions(filename):
    return [REGEX_DIR.findall(line.strip()) for line in open(filename)]


class Coord(namedtuple("Coord", "x y z", defaults=[0, 0, 0])):
    __slots__ = ()

    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y, self.z + other.z)


class HexGrid:
    DIRECTIONS = {
        "e": Coord(1, -1, 0),
        "w": Coord(-1, 1, 0),
        "se": Coord(0, -1, 1),
        "sw": Coord(-1, 0, 1),
        "ne": Coord(1, 0, -1),
        "nw": Coord(0, 1, -1),
    }

    def __init__(self):
        self.black_tiles = set()

    def walk(self, path):
        return reduce(lambda coord, dir: coord + self.DIRECTIONS[dir], path, Coord())

    def flip_tile(self, coord):
        if coord in self.black_tiles:
            self.black_tiles.remove(coord)
        else:
            self.black_tiles.add(coord)
        return self

    def count_black_tiles(self):
        return len(self.black_tiles)


def build_floor(instructions):
    return reduce(
        lambda grid, path: grid.flip_tile(grid.walk(path)), instructions, HexGrid()
    )


instructions = parse_instructions("input.txt")

print(build_floor(instructions).count_black_tiles())
