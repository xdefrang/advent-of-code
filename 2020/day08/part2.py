with open("input.txt") as f:
    input = [
        (opcode, int(operand))
        for [opcode, operand] in [line.split() for line in f.readlines()]
    ]


def detect_loop(program):
    pc = 0
    acc = 0
    visited = set()
    loop_found = False
    while True:
        (opcode, operand) = program[pc]

        visited.add(pc)

        if opcode == "acc":
            acc += operand
            pc += 1
        elif opcode == "jmp":
            pc += operand
        else:
            pc += 1

        loop_found = pc in visited

        if loop_found or pc == len(program):
            break

    return (loop_found, acc)


def patch(program, pc, instruction):
    copy = program.copy()
    copy[pc] = instruction
    return copy


def mutate(program, pc):
    (opcode, operand) = program[pc]

    next_pc = pc + 1

    if opcode == "nop":
        return (patch(program, pc, ("jmp", operand)), next_pc)
    elif opcode == "jmp":
        return (patch(program, pc, ("nop", operand)), next_pc)
    else:
        return mutate(program, next_pc)


def repair(program):
    pc = 0
    while True:
        (mutation, pc) = mutate(program, pc)
        (loop_found, acc) = detect_loop(mutation)
        if not loop_found:
            return acc


print(repair(input))
