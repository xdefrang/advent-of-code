with open("input.txt") as f:
    input = [
        (opcode, int(operand))
        for [opcode, operand] in [line.split() for line in f.readlines()]
    ]


def detect_loop(program):
    pc = 0
    acc = 0
    visited = set()

    while True:
        visited.add(pc)

        (opcode, operand) = program[pc]

        if opcode == "acc":
            acc += operand
            pc += 1
        elif opcode == "jmp":
            pc += operand
        else:
            pc += 1

        if pc in visited:
            break

    return acc


print(detect_loop(input))
