from functools import reduce
from operator import __mul__

with open("input.txt") as f:
    forest = [line.strip() for line in f.readlines()]

width, height = len(forest[0]), len(forest)


def is_tree(x, y):
    return forest[y][x % width] == "#"


def count_trees(dx, dy):
    x, y = 0, 0
    count = 0
    while y < height:
        if is_tree(x, y):
            count += 1
        x += dx
        y += dy
    return count


slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
counts = [count_trees(dx, dy) for (dx, dy) in slopes]

print(reduce(__mul__, counts))
