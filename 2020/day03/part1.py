with open("input.txt") as f:
    forest = [line.strip() for line in f.readlines()]

width, height = len(forest[0]), len(forest)


def is_tree(x, y):
    return forest[y][x % width] == "#"


x, y = 0, 0
dx, dy = 3, 1

count = 0

while y < height:
    if is_tree(x, y):
        count += 1
    x += dx
    y += dy

print(count)
