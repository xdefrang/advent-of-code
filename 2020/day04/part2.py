import re

regex_key_value = re.compile(r"([a-z]{3}):([\w#]+)")

regex_hcl = re.compile(r"^#[\da-f]{6}$")
regex_hgt = re.compile(r"^(\d+)(cm|in)$")
regex_ecl = re.compile(r"^amb|blu|brn|gry|grn|hzl|oth")
regex_pid = re.compile(r"^\d{9}$")


def valid_height(hgt):
    m = regex_hgt.match(hgt)
    if m:
        if m[2] == "cm":
            return 150 <= int(m[1]) <= 193
        else:
            return 59 <= int(m[1]) <= 76
    else:
        return False


requirements = {
    "byr": lambda x: 1920 <= int(x) <= 2002,
    "iyr": lambda x: 2010 <= int(x) <= 2020,
    "eyr": lambda x: 2020 <= int(x) <= 2030,
    "hgt": valid_height,
    "hcl": lambda x: regex_hcl.match(x),
    "ecl": lambda x: regex_ecl.match(x),
    "pid": lambda x: regex_pid.match(x),
}


def valid(passport):
    return all(
        key in passport and validator(passport[key])
        for (key, validator) in requirements.items()
    )


with open("input.txt") as f:
    passports = [
        {key: value for (key, value) in regex_key_value.findall(block)}
        for block in f.read().split("\n\n")
    ]

print(len([passport for passport in passports if valid(passport)]))
