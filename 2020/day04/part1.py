import re

regex = re.compile(r"([a-z]{3}):([\w#]+)")


with open("input.txt") as f:
    passports = [
        {key: value for (key, value) in regex.findall(block)}
        for block in f.read().split("\n\n")
    ]

required = {
    "byr",
    "iyr",
    "eyr",
    "hgt",
    "hcl",
    "ecl",
    "pid",
}

print(len([passport for passport in passports if len(required - passport.keys()) == 0]))
