import re


def parse_rule(line):
    [num, rest] = line.split(": ")
    if rest.startswith('"') and rest.endswith('"'):
        rule = rest[1:-1]
    else:
        rule = list(
            [int(r) for r in rule_nums.split(" ")] for rule_nums in rest.split(" | ")
        )

    return (int(num), rule)


def expand_r11(r42, r31, n):
    if n == 0:
        return r42 + r31
    else:
        return r42 + "(" + expand_r11(r42, r31, n - 1) + ")?" + r31


def build_regex_pattern(rules, rule_number=0):
    if rule_number == 8:
        return "(" + build_regex_pattern(rules, 42) + ")+"

    if rule_number == 11:
        r42 = build_regex_pattern(rules, 42)
        r31 = build_regex_pattern(rules, 31)
        return expand_r11(r42, r31, 10)

    rule = rules[rule_number]

    if isinstance(rule, list):
        pattern = (
            "("
            + "|".join(
                [
                    "".join([build_regex_pattern(rules, n) for n in any_rule])
                    for any_rule in rule
                ]
            )
            + ")"
        )
        return pattern
    else:
        return rule


with open("input2.txt") as f:
    [rule_lines, message_lines] = f.read().split("\n\n")

    rules = dict([parse_rule(line) for line in rule_lines.split("\n")])
    messages = [msg for msg in message_lines.split("\n") if msg != ""]

regex = re.compile("^" + build_regex_pattern(rules) + "$")

print(sum(1 for msg in messages if regex.match(msg)))
