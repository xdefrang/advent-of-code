import re


def parse_rule(line):
    [num, rest] = line.split(": ")
    if rest.startswith('"') and rest.endswith('"'):
        rule = rest[1:-1]
    else:
        rule = list(
            [int(r) for r in rule_nums.split(" ")] for rule_nums in rest.split(" | ")
        )

    return (int(num), rule)


def build_regex_pattern(rules, number=0):
    rule = rules[number]
    if isinstance(rule, list):
        return (
            "("
            + "|".join(
                [
                    "".join([build_regex_pattern(rules, n) for n in any_rule])
                    for any_rule in rule
                ]
            )
            + ")"
        )
    else:
        return rule


with open("input.txt") as f:
    [rule_lines, message_lines] = f.read().split("\n\n")

    rules = dict([parse_rule(line) for line in rule_lines.split("\n")])
    messages = [msg for msg in message_lines.split("\n") if msg != ""]

regex = re.compile("^" + build_regex_pattern(rules) + "$")

print(sum(1 for msg in messages if regex.match(msg)))
