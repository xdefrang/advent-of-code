import re

REGEX_RANGE = re.compile(r"(\d+)-(\d+)")


def parse_input(filename):
    with open(filename) as f:
        [rule_lines, my_ticket_lines, nearby_ticket_lines] = [
            section.split("\n") for section in f.read().split("\n\n")
        ]

        rules = {}
        for line in rule_lines:
            [name, rest] = line.split(":")
            ranges = [
                range(int(match[1]), int(match[2]) + 1)
                for match in REGEX_RANGE.finditer(rest)
            ]
            rules[name] = ranges

        my_ticket = parse_ticket(my_ticket_lines[1])

        nearby_tickets = [
            parse_ticket(line) for line in nearby_ticket_lines[1:] if line != ""
        ]

    return (rules, my_ticket, nearby_tickets)


def parse_ticket(line):
    return [int(n) for n in line.split(",")]


def calculate_scanning_error_rate(tickets, ranges):
    return sum(
        [n for ticket in tickets for n in ticket if not is_valid_value(n, ranges)]
    )


def is_valid_value(n, ranges):
    return any([n for r in ranges if n in r])


(rules, my_ticket, nearby_tickets) = parse_input("input.txt")

all_ranges = [rng for ranges in rules.values() for rng in ranges]

print(calculate_scanning_error_rate(nearby_tickets, all_ranges))
