import re

from functools import reduce
from operator import __mul__

REGEX_RANGE = re.compile(r"(\d+)-(\d+)")


def parse_input(filename):
    with open(filename) as f:
        [rule_lines, my_ticket_lines, nearby_ticket_lines] = [
            section.split("\n") for section in f.read().split("\n\n")
        ]

        rules = {}
        for line in rule_lines:
            [name, rest] = line.split(":")
            ranges = [
                range(int(match[1]), int(match[2]) + 1)
                for match in REGEX_RANGE.finditer(rest)
            ]
            rules[name] = ranges

        my_ticket = parse_ticket(my_ticket_lines[1])

        nearby_tickets = [
            parse_ticket(line) for line in nearby_ticket_lines[1:] if line != ""
        ]

    return (rules, my_ticket, nearby_tickets)


def parse_ticket(line):
    return [int(n) for n in line.split(",")]


def prune_invalid_tickets(tickets, ranges):
    return [ticket for ticket in tickets if is_valid_ticket(ticket, ranges)]


def is_valid_ticket(ticket, ranges):
    return all(is_valid_value(n, ranges) for n in ticket)


def is_valid_value(n, ranges):
    return any([n for r in ranges if n in r])


def detect_fields(tickets, rules):
    found = [None for field in rules]

    while any(slot is None for slot in found):
        for field, ranges in rules.items():
            if field in found:
                continue
            positions = [
                i
                for i, slot in enumerate(found)
                if slot is None
                and all(is_valid_value(ticket[i], ranges) for ticket in tickets)
            ]

            if len(positions) == 1:
                found[positions[0]] = field

    return found


(rules, my_ticket, nearby_tickets) = parse_input("input.txt")

all_ranges = [rng for ranges in rules.values() for rng in ranges]

valid_nearby_tickets = prune_invalid_tickets(nearby_tickets, all_ranges)

fields = detect_fields(valid_nearby_tickets, rules)

print(fields)

departure_values = [
    n for i, n, in enumerate(my_ticket) if fields[i].startswith("departure")
]

print(departure_values)

print(reduce(__mul__, departure_values))
