import re
from functools import reduce
from operator import __or__

regex_line = re.compile(r"^(.*) bags contain (.*)\.$")
regex_bags = re.compile(r"^\d+ (.*) bags?$")


def parse_bags(string):
    if string == "no other bags":
        return set()
    else:
        return {regex_bags.match(token)[1] for token in string.split(", ")}


with open("input.txt") as f:
    bag_rules = {
        m[1]: parse_bags(m[2])
        for m in [regex_line.match(line) for line in f.readlines()]
    }


def collect_bags(color):
    bag_colors = {
        bag_color for (bag_color, contained) in bag_rules.items() if color in contained
    }
    return reduce(
        __or__,
        [collect_bags(bag_color) for bag_color in bag_colors],
        bag_colors,
    )


print(len(collect_bags("shiny gold")))
