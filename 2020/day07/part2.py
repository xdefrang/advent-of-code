import re

regex_line = re.compile(r"^(.*) bags contain (.*)\.$")
regex_bags = re.compile(r"^(\d+) (.*) bags?$")


def parse_bags(string):
    if string == "no other bags":
        return {}
    else:
        return {
            m[2]: int(m[1])
            for m in [regex_bags.match(token) for token in string.split(", ")]
        }


with open("input.txt") as f:
    bag_rules = {
        m[1]: parse_bags(m[2])
        for m in [regex_line.match(line) for line in f.readlines()]
    }


def count_bags(color):
    return sum(
        [
            count + (count * count_bags(color))
            for (color, count) in bag_rules[color].items()
        ]
    )


print(count_bags("shiny gold"))
