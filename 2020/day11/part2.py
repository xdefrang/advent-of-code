from itertools import product
from copy import deepcopy


class SeatingChart:
    FLOOR = "."
    EMPTY = "L"
    OCCUPIED = "#"
    NEIGHBOR_OFFSETS = [
        (dx, dy)
        for (dx, dy) in product([-1, 0, 1], repeat=2)
        if not (dx == 0 and dy == 0)
    ]

    def __init__(self, seats):
        self.seats = deepcopy(seats)
        self.height = len(seats)
        self.width = len(seats[0])

    def get(self, x, y):
        return self.seats[y][x]

    def set(self, x, y, state):
        (self.seats[y])[x] = state
        return self

    def round(self):
        new_chart = SeatingChart(self.seats)
        for y in range(0, self.height):
            for x in range(0, self.width):
                current = self.get(x, y)
                occupied_neighbors = self.count(
                    self.OCCUPIED, self._collect_neighbors(x, y)
                )
                if current == self.EMPTY and occupied_neighbors == 0:
                    new_chart.set(x, y, self.OCCUPIED)
                elif current == self.OCCUPIED and occupied_neighbors >= 5:
                    new_chart.set(x, y, self.EMPTY)
        return new_chart

    def _collect_neighbors(self, x, y):
        return [
            seat
            for seat in [
                self._find_seat(x + dx, y + dy, dx, dy)
                for (dx, dy) in self.NEIGHBOR_OFFSETS
            ]
            if seat is not None
        ]

    def _find_seat(self, x, y, dx, dy):
        if 0 <= x < self.width and 0 <= y < self.height:
            s = self.get(x, y)
            if s == self.FLOOR:
                return self._find_seat(x + dx, y + dy, dx, dy)
            else:
                return s
        else:
            return None

    def count_occupied(self):
        return sum(self.count(self.OCCUPIED, row) for row in self.seats)

    @staticmethod
    def count(state, seats):
        return sum(1 for seat in seats if seat == state)

    def __eq__(self, other):
        return self.seats == other.seats

    def __str__(self):
        return "\n".join(["".join(row) for row in self.seats])


seating_chart = SeatingChart(
    [[char for char in line.strip()] for line in open("input.txt")]
)

previous_seating_chart = None
while True:
    previous_seating_chart = seating_chart
    seating_chart = seating_chart.round()

    if seating_chart == previous_seating_chart:
        print(seating_chart.count_occupied())
        break
