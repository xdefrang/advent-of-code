import re
from functools import reduce
from operator import __mul__


class Tile:
    def __init__(self, id, rows):
        self.id = id
        self.borders = {
            rows[0],
            rows[0][::-1],
            "".join([row[-1] for row in rows]),
            "".join([row[-1] for row in rows][::-1]),
            rows[-1],
            rows[-1][::-1],
            "".join([row[0] for row in rows]),
            "".join([row[0] for row in rows][::-1]),
        }


def parse_input(filename):
    tiles = {}
    with open(filename) as f:
        for section in f.read().split("\n\n"):
            lines = section.split("\n")
            match = re.match(r"Tile (\d+):", lines[0])
            tile_id = int(match[1])
            tile = Tile(tile_id, [line for line in lines[1:] if line != ""])
            tiles[tile_id] = tile
    return tiles


def find_corners(tiles):
    found = set()
    for tile in tiles.values():
        matching = set()
        for other_tile in tiles.values():
            if tile.id == other_tile.id:
                continue
            if len(tile.borders.intersection(other_tile.borders)) > 0:
                matching.add(other_tile.id)
        if len(matching) == 2:
            found.add(tile.id)
    return found


tiles = parse_input("input.txt")

print(reduce(__mul__, find_corners(tiles)))
