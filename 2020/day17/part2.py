from itertools import product

NEIGHBOR_DIRECTIONS = [
    vector for vector in product([-1, 0, 1], repeat=4) if vector != (0, 0, 0, 0)
]


def add(point, vector):
    return tuple(pi + vi for pi, vi in zip(point, vector))


def count_active_neighbours(point, space):
    return sum(
        [1 for direction in NEIGHBOR_DIRECTIONS if add(point, direction) in space]
    )


def bounds(space, dimension):
    values = [point[dimension] for point in space]
    return (min(values) - 1, max(values) + 2)


def next_generation(space):
    bounds_x, bounds_y, bounds_z, bounds_w = (
        bounds(space, 0),
        bounds(space, 1),
        bounds(space, 2),
        bounds(space, 3),
    )
    all_points_in_space = [
        (x, y, z, w)
        for x in range(*bounds_x)
        for y in range(*bounds_y)
        for z in range(*bounds_z)
        for w in range(*bounds_w)
    ]

    new_space = set()

    for p in all_points_in_space:
        n = count_active_neighbours(p, space)
        if p in space:
            if n in (2, 3):
                new_space.add(p)
        else:
            if n == 3:
                new_space.add(p)

    return new_space


space = set(
    (x, y, 0, 0)
    for (y, line) in enumerate(open("input.txt"))
    for (x, char) in enumerate(line.strip())
    if char == "#"
)

for i in range(0, 6):
    space = next_generation(space)

print(len(space))
