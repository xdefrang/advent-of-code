INPUT = [3, 6, 8, 1, 9, 5, 7, 4, 2]


def wrap(x, n):
    return n if x < 1 else x


def play(cups, num_moves):
    num_cups = len(cups)
    current_label = None

    for _ in range(num_moves):
        current_index = (
            (cups.index(current_label) + 1) % num_cups if current_label else 0
        )
        current_label = cups[current_index]

        picked_up = [
            cups[(current_index + offset) % num_cups] for offset in range(1, 4)
        ]
        for cup in picked_up:
            cups.remove(cup)

        destination_label = wrap(current_label - 1, num_cups)
        while destination_label in picked_up:
            destination_label = wrap(destination_label - 1, num_cups)

        destination_index = cups.index(destination_label)
        insertion_index = destination_index + 1

        cups = cups[:insertion_index] + picked_up + cups[insertion_index:]

    return cups


print(play(INPUT, 100))
