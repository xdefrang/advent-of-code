from itertools import chain


class Node:
    def __init__(self, value):
        self.value = value
        self.prev = None
        self.next = None


class Ring:
    def __init__(self):
        self.head = None
        self.nodes_map = {}

    def build(self, values):
        self.nodes_map = {}
        self.head = prev = None
        for value in values:
            node = Node(value)
            if self.head is None:
                self.head = node
            if prev:
                prev.next = node
                node.prev = prev
            self.nodes_map[value] = node
            prev = node
        self.head.prev = prev
        prev.next = self.head
        return self

    def find_node(self, value):
        return self.nodes_map[value]

    def move_after(self, node, destination):
        node, destination = self.nodes_map[node], self.nodes_map[destination]

        prev_next = destination.next

        node.prev.next = node.next
        node.next.prev = node.prev

        destination.next.prev = node
        destination.next = node

        node.prev = destination
        node.next = prev_next

    def __len__(self):
        return len(self.nodes_map)


def wrap(x, n):
    return n if x < 1 else x


def play(cups, num_moves):
    current_cup, num_cups = cups.head, len(cups)
    for _ in range(num_moves):
        picked_up = [
            current_cup.next.value,
            current_cup.next.next.value,
            current_cup.next.next.next.value,
        ]

        destination = wrap(current_cup.value - 1, num_cups)
        while destination in picked_up:
            destination = wrap(destination - 1, num_cups)

        cups.move_after(picked_up[2], destination)
        cups.move_after(picked_up[1], destination)
        cups.move_after(picked_up[0], destination)

        current_cup = current_cup.next

    return cups


INPUT = [3, 6, 8, 1, 9, 5, 7, 4, 2]

cups = Ring().build(chain(iter(INPUT), range(10, 1_000_000 + 1)))

cups = play(cups, 10_000_000)

one = cups.find_node(1)

print(one.next.value * one.next.next.value)
