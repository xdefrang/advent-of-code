from functools import reduce


def transform(n, sn):
    return (n * sn) % 20201227


def find_loop_size(pubkey, subject_number):
    loop_size, value = 0, 1
    while value != pubkey:
        loop_size += 1
        value = transform(value, subject_number)
    return loop_size


def handshake(subject_number, loop_size):
    return reduce(lambda key, _: transform(key, subject_number), range(loop_size), 1)


card_pubkey, door_pubkey = 13316116, 13651422

print(handshake(door_pubkey, find_loop_size(card_pubkey, 7)))
