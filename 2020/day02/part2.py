import re

regex = re.compile(r"(\d+)-(\d+) ([a-z]): (.*)")

with open("input.txt") as f:
    input = [
        (int(m[1]), int(m[2]), m[3], m[4])
        for m in [regex.search(line) for line in f.readlines()]
    ]

    valid = [
        password
        for (p1, p2, letter, password) in input
        if (password[p1 - 1] == letter) != (password[p2 - 1] == letter)
    ]
    print(len(valid))
