import re

regex = re.compile(r"(\d+)-(\d+) ([a-z]): (.*)")

with open("input.txt") as f:
    input = [
        (int(m[1]), int(m[2]), m[3], m[4])
        for m in [regex.search(line) for line in f.readlines()]
    ]
    valid = [
        password
        for (min, max, letter, password) in input
        if min <= password.count(letter) <= max
    ]
    print(len(valid))
