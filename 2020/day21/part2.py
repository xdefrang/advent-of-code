import re


def find_dangerous_ingredients(food):
    allergen_ingredients = {}

    for ingredients, allergens in food:
        for allergen in allergens:
            if allergen not in allergen_ingredients:
                allergen_ingredients[allergen] = set(ingredients)
            else:
                allergen_ingredients[allergen] &= set(ingredients)

    unique_allergen_ingredients = {}
    while any([len(ingredients) > 0 for ingredients in allergen_ingredients.values()]):
        for allergen, ingredients in allergen_ingredients.items():
            if len(ingredients) == 1:
                unique_allergen_ingredients[allergen] = ingredients.pop()
        ingredients_found = set(unique_allergen_ingredients.values())
        for allergen in allergen_ingredients:
            allergen_ingredients[allergen] -= ingredients_found

    return [
        ingredient
        for allergen, ingredient in sorted(unique_allergen_ingredients.items())
    ]


REGEX_LINE = re.compile(r"([a-z ]+) \(contains ([^\)]+)\)")

food = []
for line in open("input.txt"):
    match = REGEX_LINE.match(line)
    allergens = match[2].split(", ")
    ingredients = match[1].split(" ")

    food.append((ingredients, allergens))

print(",".join(find_dangerous_ingredients(food)))
