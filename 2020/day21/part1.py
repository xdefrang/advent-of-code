from functools import reduce
from operator import __or__
import re


def count_safe_ingredients(food):
    all_ingredients = []
    allergen_ingredients = {}

    for ingredients, allergens in food:
        all_ingredients.extend(ingredients)
        for allergen in allergens:
            if allergen not in allergen_ingredients:
                allergen_ingredients[allergen] = set(ingredients)
            else:
                allergen_ingredients[allergen] &= set(ingredients)

    ingredients_with_allergens = reduce(__or__, allergen_ingredients.values())

    return sum(
        1
        for ingredient in all_ingredients
        if ingredient not in ingredients_with_allergens
    )


REGEX_LINE = re.compile(r"([a-z ]+) \(contains ([^\)]+)\)")

food = []
for line in open("input.txt"):
    match = REGEX_LINE.match(line)
    allergens = match[2].split(", ")
    ingredients = match[1].split(" ")

    food.append((ingredients, allergens))


print(count_safe_ingredients(food))
