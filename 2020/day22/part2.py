class Deck:
    def __init__(self, id, cards):
        self.id = id
        self.cards = cards

    def empty(self):
        return len(self.cards) == 0

    def __len__(self):
        return len(self.cards)

    def take(self):
        return self.cards.pop(0)

    def slice(self, n):
        return Deck(self.id, self.cards[:n])

    def put(self, card):
        self.cards.append(card)
        return self

    def score(self):
        return sum(pos * value for pos, value in enumerate(self.cards[::-1], start=1))

    def __repr__(self):
        return f"Deck({self.id}, {repr(self.cards)})"


def recursive_combat(deck1, deck2):
    winner = None
    seen = set()

    while winner is None:
        decks = (repr(deck1), repr(deck2))

        if decks in seen:
            return deck1

        seen.add(decks)

        c1, c2 = deck1.take(), deck2.take()

        if c1 <= len(deck1) and c2 <= len(deck2):
            round_winner = recursive_combat(deck1.slice(c1), deck2.slice(c2)).id
        else:
            round_winner = c1 > c2 and 1 or 2

        if round_winner == 1:
            deck1.put(c1).put(c2)
            if deck2.empty():
                winner = deck1
        else:
            deck2.put(c2).put(c1)
            if deck1.empty():
                winner = deck2

        # input()

    return winner


player1 = Deck(
    1,
    [
        12,
        48,
        26,
        22,
        44,
        16,
        31,
        19,
        30,
        10,
        40,
        47,
        21,
        27,
        2,
        46,
        9,
        15,
        23,
        6,
        50,
        28,
        5,
        42,
        34,
    ],
)
player2 = Deck(
    2,
    [
        14,
        45,
        4,
        24,
        1,
        7,
        36,
        29,
        38,
        33,
        3,
        13,
        11,
        17,
        39,
        43,
        8,
        41,
        32,
        37,
        35,
        49,
        20,
        18,
        25,
    ],
)

winner = recursive_combat(player1, player2)

print(winner.score())
