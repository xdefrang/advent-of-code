class Deck:
    def __init__(self, cards):
        self.cards = cards

    def empty(self):
        return len(self.cards) == 0

    def __len__(self):
        return len(self.cards)

    def take(self):
        return self.cards.pop(0)

    def put(self, card):
        self.cards.append(card)
        return self

    def score(self):
        return sum(pos * value for pos, value in enumerate(self.cards[::-1], start=1))

    def __repr__(self):
        return f"Deck({repr(self.cards)})"


def combat(deck1, deck2):
    winner = None

    while winner is None:
        c1, c2 = deck1.take(), deck2.take()

        if c1 > c2:
            deck1.put(c1).put(c2)
            if deck2.empty():
                winner = deck1
        else:
            deck2.put(c2).put(c1)
            if deck1.empty():
                winner = deck2

    return winner.score()


player1 = Deck(
    [
        12,
        48,
        26,
        22,
        44,
        16,
        31,
        19,
        30,
        10,
        40,
        47,
        21,
        27,
        2,
        46,
        9,
        15,
        23,
        6,
        50,
        28,
        5,
        42,
        34,
    ]
)
player2 = Deck(
    [
        14,
        45,
        4,
        24,
        1,
        7,
        36,
        29,
        38,
        33,
        3,
        13,
        11,
        17,
        39,
        43,
        8,
        41,
        32,
        37,
        35,
        49,
        20,
        18,
        25,
    ]
)

print(combat(player1, player2))
