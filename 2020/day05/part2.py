from itertools import tee
from functools import reduce

DIGITS = {"F": 0, "B": 1, "L": 0, "R": 1}


def seat_id(string):
    return reduce(lambda acc, char: (acc << 1) + DIGITS[char], string, 0)


def row(sid):
    return sid >> 3


def each_cons(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


with open("input.txt") as f:
    seat_ids = [seat_id(boarding_pass.strip()) for boarding_pass in f.readlines()]

    candidates = [sid for sid in seat_ids if 0 < row(sid) < 127]

    for (sid1, sid2) in each_cons(sorted(candidates)):
        if (sid2 - sid1) == 2:
            print(sid1 + 1)
