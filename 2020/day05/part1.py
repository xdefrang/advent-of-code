from functools import reduce

DIGITS = {"F": 0, "B": 1, "L": 0, "R": 1}


def seat_id(string):
    return reduce(lambda acc, char: (acc << 1) + DIGITS[char], string, 0)


with open("input.txt") as f:
    print(max([seat_id(boarding_pass.strip()) for boarding_pass in f.readlines()]))
