from collections import defaultdict, deque


class Game:
    def __init__(self, starting_numbers):
        self._starting_numbers = deque(starting_numbers[:])
        self._seen_numbers = defaultdict(list)
        self._last_number = 0
        self._turn = 0

    def play_turn(self):
        self._turn += 1

        if len(self._starting_numbers) > 0:
            current_number = self._starting_numbers.popleft()
        else:
            seen_at_turns = self._seen_numbers.get(self._last_number, [])
            if len(seen_at_turns) > 1:
                current_number = seen_at_turns[1] - seen_at_turns[0]
                seen_at_turns.pop(0)
            else:
                current_number = 0

        self._seen_numbers[current_number].append(self._turn)

        self._last_number = current_number
        return current_number


game = Game([11, 18, 0, 20, 1, 7, 16])

number = 0
for t in range(0, 30000000):
    number = game.play_turn()
print(number)
