instructions = [(line[0], int(line[1:-1])) for line in open("input.txt")]


class Ship:

    ESWN = ((1, 0), (0, -1), (-1, 0), (0, 1))

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        self.d = 0

    def perform(self, action, units):
        if action == "L":
            self.rotate(-units)
        if action == "R":
            self.rotate(units)
        if action == "F":
            (dx, dy) = self.ESWN[self.d]
            self.x += dx * units
            self.y += dy * units
        if action == "N":
            self.y += units
        if action == "S":
            self.y -= units
        if action == "E":
            self.x += units
        if action == "W":
            self.x -= units

    def rotate(self, units):
        self.d = (self.d + (units // 90)) % 4

    def distance(self):
        return abs(self.x) + abs(self.y)


s = Ship()
for (action, units) in instructions:
    s.perform(action, units)

print(s.distance())
