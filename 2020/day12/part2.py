instructions = [(line[0], int(line[1:-1])) for line in open("input.txt")]


class Ship:

    ESWN = ((1, 0), (0, -1), (-1, 0), (0, 1))

    def __init__(self, x=0, y=0, wx=10, wy=1):
        self.x = x
        self.y = y
        self.wx = wx
        self.wy = wy

    def perform(self, action, units):
        if action == "L":
            (self.wx, self.wy) = self.rotate(-units)
        if action == "R":
            (self.wx, self.wy) = self.rotate(units)
        if action == "F":
            self.x += self.wx * units
            self.y += self.wy * units
        if action == "N":
            self.wy += units
        if action == "S":
            self.wy -= units
        if action == "E":
            self.wx += units
        if action == "W":
            self.wx -= units

    def rotate(self, units):
        if units in (90, -270):
            return (self.wy, -self.wx)
        elif abs(units) == 180:
            return (-self.wx, -self.wy)
        elif units in (270, -90):
            return (-self.wy, self.wx)

    def distance(self):
        return abs(self.x) + abs(self.y)


s = Ship()
for (action, units) in instructions:
    s.perform(action, units)

print(s.distance())
