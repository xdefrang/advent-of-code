INPUT = open("input.txt").read()

print(
    sum(
        sorted(
            sum(
                map(
                    int,
                    text.strip().split("\n"),
                ),
            )
            for text in INPUT.split("\n\n")
        )[-3:]
    )
)
