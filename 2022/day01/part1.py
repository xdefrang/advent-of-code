INPUT = open("input.txt").read()

print(
    max(
        sum(
            map(
                int,
                text.strip().split("\n"),
            ),
        )
        for text in INPUT.split("\n\n")
    )
)
