from enum import Enum


class Outcome(Enum):
    WIN = 6
    DRAW = 3
    LOSS = 0

    @classmethod
    def parse(cls, letter):
        match letter:
            case "X":
                return cls.LOSS
            case "Y":
                return cls.DRAW
            case "Z":
                return cls.WIN


class Shape(Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3

    def guess(self, outcome):
        if outcome == Outcome.DRAW:
            return self

        if outcome == Outcome.LOSS:
            match self:
                case self.ROCK:
                    return self.SCISSORS
                case self.PAPER:
                    return self.ROCK
                case self.SCISSORS:
                    return self.PAPER

        if outcome == Outcome.WIN:
            match self:
                case self.ROCK:
                    return self.PAPER
                case self.PAPER:
                    return self.SCISSORS
                case self.SCISSORS:
                    return self.ROCK

    @classmethod
    def parse(cls, letter):
        match letter:
            case "A":
                return cls.ROCK
            case "B":
                return cls.PAPER
            case "C":
                return cls.SCISSORS


INPUT = [
    tuple(
        parser(char)
        for (char, parser) in zip(line.split(), (Shape.parse, Outcome.parse))
    )
    for line in open("input.txt").readlines()
]

print(sum(them.guess(outcome).value + outcome.value for them, outcome in INPUT))
