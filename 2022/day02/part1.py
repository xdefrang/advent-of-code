from enum import Enum


class Outcome(Enum):
    WIN = 6
    DRAW = 3
    LOSS = 0

    @classmethod
    def from_bool(cls, win):
        return cls.WIN if win else cls.LOSS


class Shape(Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3

    def vs(self, other):
        if self == other:
            return Outcome.DRAW

        match self:
            case self.ROCK:
                return Outcome.from_bool(other == self.SCISSORS)
            case self.PAPER:
                return Outcome.from_bool(other == self.ROCK)
            case self.SCISSORS:
                return Outcome.from_bool(other == self.PAPER)

    @classmethod
    def parse(cls, letter):
        match letter:
            case "A" | "X":
                return cls.ROCK
            case "B" | "Y":
                return cls.PAPER
            case "C" | "Z":
                return cls.SCISSORS


INPUT = [
    tuple(map(Shape.parse, line.split())) for line in open("input.txt").readlines()
]

print(sum(us.value + us.vs(them).value for them, us in INPUT))
