import itertools
import pathlib


REPORTS = [
    list(map(int, line.split()))
    for line in pathlib.Path("input.txt").read_text().strip().split("\n")
]


def sign(x):
    return -1 if x < 0 else 1


def is_safe(levels) -> bool:
    deltas = [a - b for a, b in itertools.pairwise(levels)]
    expected_sign = sign(deltas[0])
    if not all(sign(x) == expected_sign for x in deltas[1:]):
        return False
    for x in map(abs, deltas):
        if x < 1 or x > 3:
            return False
    return True


def is_safe2(levels) -> bool:
    if is_safe(levels):
        return True
    for idx in range(len(levels)):
        sublevels = levels[:idx] + levels[idx + 1 :]
        if is_safe(sublevels):
            return True
    return False


print(sum(is_safe2(levels) for levels in REPORTS))
