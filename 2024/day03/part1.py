import pathlib
import re

INSTRUCTIONS = pathlib.Path("input.txt").read_text().strip()

print(
    sum(
        int(left) * int(right)
        for left, right in re.findall(r"mul\((\d{1,3}),(\d{1,3})\)", INSTRUCTIONS)
    )
)
