import pathlib
import re

INSTRUCTIONS = pathlib.Path("input.txt").read_text().strip()

do, result = True, 0
for instruction, left, right in re.findall(
    r"(do(?:n't)?\(\)|mul\((\d{1,3}),(\d{1,3})\))", INSTRUCTIONS
):
    match instruction:
        case "do()":
            do = True
        case "don't()":
            do = False
        case _ if do:
            result += int(left) * int(right)

print(result)
