import collections
import pathlib

left, right_occurences = [], collections.Counter()
for line in pathlib.Path("input.txt").read_text().strip().split("\n"):
    l, r = line.split()
    left.append(int(l))
    right_occurences[int(r)] += 1

# Part 2
print(sum(l * right_occurences[l] for l in left))
