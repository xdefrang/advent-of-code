import pathlib

left, right = [], []
for line in pathlib.Path("input.txt").read_text().strip().split("\n"):
    l, r = line.split()
    left.append(int(l))
    right.append(int(r))

# Part 1
print(sum(abs(l - r) for l, r in zip(sorted(left), sorted(right))))
